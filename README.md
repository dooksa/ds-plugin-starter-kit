# Dooksa plugin starter kit

This plugin is a template to start development of an external plugin

### Setup config file

Edit plugins **name**, **filename**, and **version** in **ds.plugin.config.js**

### Install dependencies

```
$ npm install
```

### Update dooksa packages

To update dooksa private packages you must run the following script

```
$ npm run update
```

### Development

The development environment is a mock ds-app setup managed by ds-plugin-manager

In order to test public methods you must use the this.$method function within the dsApp.setup function on line 15

The development directory is **/dev**

```
$ npm run dev
```

### Run tests


Unit tests

```
$ npm run test:unit
```
```
$ npm run test:unit:record
```

Integration tests

The e2e directory is **/test**

```
$ npm run test:e2e
```
```
$ npm run test:e2e:record
```
### Build with hash

This will build the external plugin

```
$ npm run build
```