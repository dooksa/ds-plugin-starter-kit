/**
 * Ds Plugin.
 * @module plugin
 */
export default {
  name: 'dsStarterKit',
  version: 1,
  dependencies: [
    // (Optional) Dependencies is a list of external dependent plugins
  ],
  data: {
    // (Optional) Data is a object of shared data
  },
  setup () {
    // (Optional) Setup initialises the plugin
  },
  methods: {
    // (Optional) Methods contains the plugins functions
    /**
     * @param {Object} context - This object is used to identify what method ran this function
     * @param {string} context.name - Name of the plugin
     * @param {string} context.version - Version of the plugin
     * @param {Array} context.dependencies - List of the of the plugins dependencies
     * @param {*} params - Params are the parameters used for this method, the param should be an object if the public method needs separated values
     */
    publicMethod (context, params) {
      // public method
      console.log(params)
    },
    /**
     * @param {*} params - Params are the parameters used for this method
     */
    _privateMethod (params) {
      // private method name starts with '_'
    }
  }
}
