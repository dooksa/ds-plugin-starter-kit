import DsPlugin from '@dooksa/ds-plugin'
import DsManager from '@dooksa/ds-plugin-manager'
import myPlugin from '../src'

const dsManager = new DsPlugin(DsManager, [])
const dsApp = {
  name: 'dsApp',
  version: 1,
  dependencies: [
    {
      name: myPlugin.name,
      version: myPlugin.version
    }
  ],
  setup () {
    // test myPlugin by running its public methods
    this.$method('dsStarterKit/publicMethod', 'test')
  }
}

dsManager.init({
  buildId: 1,
  plugins: [
    {
      item: {
        name: myPlugin.name,
        version: myPlugin.version
      },
      plugin: myPlugin,
      options: {
        setupOnRequest: false
      }
    },
    {
      item: {
        name: dsApp.name,
        version: dsApp.version
      },
      plugin: dsApp,
      options: {
        setupOnRequest: false
      }
    }
  ],
  isDev: true
})
